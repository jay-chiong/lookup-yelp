package com.personal.yelp.lookup.repository.model

data class User(
    val id: String,
    val profile_url: String,
    val image_url: String,
    val name: String
)
