package com.personal.yelp.lookup.repository.model

data class AutoCompleteResult(
    val categories: ArrayList<Categories>,
    val businesses: ArrayList<Businesses>,
    val terms: ArrayList<Terms>
)
