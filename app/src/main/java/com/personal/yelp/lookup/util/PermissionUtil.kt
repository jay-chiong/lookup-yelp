package com.personal.yelp.lookup.util

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.location.LocationManager
import android.os.Build
import android.provider.Settings
import androidx.core.app.ActivityCompat

class PermissionUtil
{
    fun isLocationGrantedAndEnabled(context: Context) : Boolean
    {
        return isLocationPermissionsGranted(context) && isSystemLocationEnabled(context)
    }

    fun isLocationPermissionsGranted(context: Context) : Boolean
    {
        if (ActivityCompat.checkSelfPermission(
                context, Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                context, Manifest.permission.ACCESS_COARSE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
        )
        {
            return true
        }

        return false
    }

    fun isSystemLocationEnabled(context: Context) : Boolean
    {
        var gpsEnabled = false
        var networkEnabled = false

        val locationMgr = context.getSystemService(Context.LOCATION_SERVICE) as LocationManager

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P)
        {
            try
            {
                gpsEnabled = locationMgr.isProviderEnabled(LocationManager.GPS_PROVIDER)
            } catch (ex: Exception){}

            try {
                networkEnabled = locationMgr.isProviderEnabled(LocationManager.NETWORK_PROVIDER)
            } catch (ex: Exception){}

            return gpsEnabled && networkEnabled
        }
        else
        {
            var locationMode = 0

            try {
                locationMode = Settings.Secure.getInt(context.contentResolver, Settings.Secure.LOCATION_MODE)
            } catch (e: Settings.SettingNotFoundException) {
                e.printStackTrace()
                return false
            }

            return locationMode !== Settings.Secure.LOCATION_MODE_OFF
        }

        return false
    }
}