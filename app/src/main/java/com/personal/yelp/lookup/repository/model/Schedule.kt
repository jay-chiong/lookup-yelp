package com.personal.yelp.lookup.repository.model

import java.text.DateFormatSymbols
import java.text.SimpleDateFormat
import java.util.*

data class Schedule(
    val is_overnight: Boolean,
    val start: String,
    val end: String,
    val day: Int
)
{
    fun getWorkTime() : String
    {
        return getWorkDay() + ": " + getStartTime() + " - " + getEndTime()
    }

    private fun getWorkDay() : String
    {
        val weekdays = DateFormatSymbols.getInstance().weekdays
        return weekdays[day + 1]
    }

    private fun getStartTime() : String
    {
        val militaryTimeFormatter = SimpleDateFormat("HHmm", Locale.ENGLISH)
        val ordinaryTimeFormatter = SimpleDateFormat("hh:mm a", Locale.ENGLISH)
        val date = militaryTimeFormatter.parse(start.toString())

        return ordinaryTimeFormatter.format(date)
    }

    private fun getEndTime() : String
    {
        val militaryTimeFormatter = SimpleDateFormat("HHmm", Locale.ENGLISH)
        val ordinaryTimeFormatter = SimpleDateFormat("hh:mm a", Locale.ENGLISH)
        val date = militaryTimeFormatter.parse(end.toString())

        return ordinaryTimeFormatter.format(date)
    }
}
