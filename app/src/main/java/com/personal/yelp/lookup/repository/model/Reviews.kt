package com.personal.yelp.lookup.repository.model

import android.util.Log
import java.text.SimpleDateFormat
import java.util.*

data class Reviews(
    val id: String,
    val rating: Float,
    val user: User,
    val text: String,
    val time_created: String,
    val url: String
)
{
    fun getParsedDate() : String
    {
        var parsedDate: String

        val dateTimeFormatter = SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)
        val dateFormatter = SimpleDateFormat("MM-dd-yyyy", Locale.ENGLISH)

        val date = dateTimeFormatter.parse(time_created)
        parsedDate = dateFormatter.format(date)

        return parsedDate
    }
}