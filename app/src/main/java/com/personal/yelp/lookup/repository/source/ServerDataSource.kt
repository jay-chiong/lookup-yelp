package com.personal.yelp.lookup.repository.source

import android.content.Context
import androidx.annotation.NonNull
import com.personal.yelp.lookup.repository.model.AutoCompleteResult
import com.personal.yelp.lookup.repository.model.BusinessReview
import com.personal.yelp.lookup.repository.model.Businesses
import com.personal.yelp.lookup.repository.model.SearchResult
import okhttp3.*
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*

interface ServerDataSource
{
    @GET("autocomplete")
    fun getAutocomplete(
        @Query("text") @NonNull text: String,
        @Query("location") @NonNull location: String
    ) : Call<AutoCompleteResult>

    @GET("businesses/search")
    fun searchBusiness(
        @Query("term") @NonNull term: String,
        @Query("location") @NonNull location: String,
        @Query("categories") @NonNull categories: String
    ) : Call<SearchResult>

    @GET("businesses/{id}")
    fun getBusinessDetails(
        @Path("id") @NonNull id: String
    ) : Call<Businesses>

    @GET("businesses/{id}/reviews")
    fun getBusinessReviews(
        @Path("id") @NonNull id: String
    ) : Call<BusinessReview>

    companion object
    {
        var BASE_URL = "https://api.yelp.com/v3/"

        private const val cacheSize = (10 * 1024 * 1024).toLong()

        fun create(context: Context) : ServerDataSource
        {

            val okHttpClient = createClientBuilder(context).build()
            val retrofit = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(BASE_URL)
                .client(okHttpClient)
                .build()
            return retrofit.create(ServerDataSource::class.java)

        }

        private fun createClientBuilder(context: Context) : OkHttpClient.Builder
        {
            val myCache = Cache(context.cacheDir, cacheSize)
            val httpClient = OkHttpClient.Builder()

            httpClient.addInterceptor { chain ->
                val original = chain.request()

                val request = original.newBuilder()
                    .addHeader(
                        "Authorization",
                        "Bearer BOqa6EucunSC2Gd3rKUGYjNuxUw-XknPKUI9cbCYZl2iboaI1CmO36Ex5iN0loQlCY43hY8y2OWx88v2Y7rgTLDYmLwORdj6eP6pShjcrboP9Qe2TWt4EkTARyWtYHYx"
                    )
                    .method(original.method(), original.body())
                    .build()

                chain.proceed(request)
            }

            httpClient.cache(myCache)

            return httpClient
        }
    }
}