package com.personal.yelp.lookup.repository.model

data class BusinessReview(
    val reviews: ArrayList<Reviews>
)
