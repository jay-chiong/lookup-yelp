package com.personal.yelp.lookup.repository.model

data class Coordinates(
    val latitude: Double,
    val longitude: Double
)
