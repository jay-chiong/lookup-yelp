package com.personal.yelp.lookup.repository.model

data class Rating(
    val Source: String,
    val Value: String
)
