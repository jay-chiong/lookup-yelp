package com.personal.yelp.lookup.repository.model

data class Location(
    val address1: String,
    val address2: String,
    val address3: String,
    val city: String,
    val zip_code: String,
    val country: String,
    val state: String,
    val display_address: ArrayList<String>
)
{
    fun getFullAddress() : String
    {
        var address = ""

        for (a in display_address)
        {
            address += if (address.isEmpty())
                            a
                        else
                            ", $a"
        }

        return address
    }
}