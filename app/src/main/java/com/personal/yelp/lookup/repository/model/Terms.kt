package com.personal.yelp.lookup.repository.model

data class Terms(
    val text: String
)
