package com.personal.yelp.lookup.search

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.provider.Settings
import android.view.*
import androidx.core.content.ContextCompat
import androidx.databinding.Observable
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.gms.location.*
import com.mancj.materialsearchbar.MaterialSearchBar
import com.mancj.materialsearchbar.MaterialSearchBar.OnSearchActionListener
import com.personal.yelp.lookup.R
import com.personal.yelp.lookup.common.ViewModelFactory
import com.personal.yelp.lookup.databinding.FragmentSearchBinding
import com.personal.yelp.lookup.util.PermissionUtil


/**
 * Fragment class for showing the search results
 */
class SearchFragment : Fragment(), OnSearchActionListener
{
    private lateinit var _binding: FragmentSearchBinding
    private lateinit var _searchViewModel: SearchViewModel
    private lateinit var _searchAdapter: SearchAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root: View = inflater.inflate(R.layout.fragment_search, container, false)

        _binding = FragmentSearchBinding.bind(root)

        setupViewModel()

        return _binding.root
    }

    override fun onResume()
    {
        super.onResume()

        checkForPermission()
    }

    override fun onSearchStateChanged(enabled: Boolean)
    {
        _searchViewModel.showOtherSearchViews.set(enabled)

        if (enabled)
        {
            _binding.termSearchBar.text = _searchViewModel.currentTextInSearch
            Handler(Looper.getMainLooper()).postDelayed({
                _binding.termSearchBar.setNavIconTint(ContextCompat.getColor(requireContext(), R.color.black))
            }, 200)
        }
        else
        {
            _binding.termSearchBar.setNavIconTint(ContextCompat.getColor(requireContext(), android.R.color.transparent))
        }
    }

    override fun onSearchConfirmed(text: CharSequence?)
    {
        saveSearchTermAndUpdatePlaceholderText()
        _binding.shimmerView.startShimmer()
        _searchViewModel.getSearchResult(text.toString())
        _binding.termSearchBar.clearFocus()
        _searchViewModel.showOtherSearchViews.set(false)
        _binding.termSearchBar.closeSearch()
    }

    override fun onButtonClicked(buttonCode: Int)
    {
        when (buttonCode)
        {
            MaterialSearchBar.BUTTON_BACK -> {
                _searchViewModel.currentTextInSearch = _binding.termSearchBar.text
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        if(requestCode == 1)
        {
            if(grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)
            {

            }
        }
    }

    private fun setupViewModel()
    {
        _searchViewModel = getSearchResultViewModel()
        _binding.viewModel = _searchViewModel

        _searchViewModel.fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(requireActivity())

        _searchAdapter = SearchAdapter(_searchViewModel.searchItems, requireActivity())
        _binding.resultsRv.layoutManager = LinearLayoutManager(requireActivity(), LinearLayoutManager.VERTICAL, false)
        _binding.resultsRv.adapter = _searchAdapter
        
        _searchAdapter.onItemClick = { business ->
            _searchViewModel.setSelectedBusiness(business)
            findNavController().navigate(R.id.action_SearchResultFragment_to_detailsFragment)
        }

        _searchViewModel.checkPermissionEvent.observe(this, {
            checkForPermission()
        })

        _searchViewModel.launchLocationSettingsEvent.observe(this, {
            launchLocationSettings()
        })

        _searchViewModel.doSearch.observe(this, Observer {
            saveSearchTermAndUpdatePlaceholderText()
            _searchViewModel.getSearchResult(_binding.termSearchBar.text)
            _binding.termSearchBar.closeSearch()
        })

        _searchViewModel.notifyAdapterEvent.observe(this, {
            _searchAdapter.notifyDataSetChanged()
        })

        _searchViewModel.isReadyToDisplay.addOnPropertyChangedCallback(object :
            Observable.OnPropertyChangedCallback() {
            override fun onPropertyChanged(sender: Observable?, propertyId: Int) {
                if (_searchViewModel.isReadyToDisplay.get())
                    _binding.shimmerView.stopShimmer()
            }
        })

        _searchViewModel.searchItemsAddOnListChanged()

        _binding.termSearchBar.setOnSearchActionListener(this)
        _binding.termSearchBar.setCardViewElevation(10)
        _binding.locationBar.setOnEditorActionListener(_searchViewModel)
        _binding.categoriesBar.setOnEditorActionListener(_searchViewModel)
    }

    private fun checkForPermission()
    {
        if (!PermissionUtil().isLocationPermissionsGranted(requireContext()))
        {
            // request for location permission
            requestPermissions(
                arrayOf(
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ), 1
            )
        }
        else if (!PermissionUtil().isSystemLocationEnabled(requireContext()))
        {
            _searchViewModel.showGpsRequestDialog()
        }
        else
        {
            _searchViewModel.getLastLocation()
        }
    }

    private fun launchLocationSettings()
    {
        try
        {
            startActivity(Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS))
        } catch (e: Exception){}
    }

    private fun saveSearchTermAndUpdatePlaceholderText()
    {
        _binding.termSearchBar.placeHolderView.text = _binding.termSearchBar.text
        _searchViewModel.currentTextInSearch = _binding.termSearchBar.text
    }

    private fun getSearchResultViewModel() : SearchViewModel
    {
        val factory = ViewModelFactory(requireActivity().application, requireActivity()).getInstance()

        return ViewModelProvider(requireActivity(), factory).get(SearchViewModel::class.java)
    }
}