package com.personal.yelp.lookup.details

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.databinding.ObservableArrayList
import androidx.recyclerview.widget.RecyclerView
import com.personal.yelp.lookup.R
import com.squareup.picasso.Picasso

class PhotoAdapter(private val photo_url: ObservableArrayList<String>) :
    RecyclerView.Adapter<PhotoAdapter.ViewHolder>()
{
    class ViewHolder(view: View) : RecyclerView.ViewHolder(view)
    {
        val imageView: ImageView = view.findViewById(R.id.business_photo_iv)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder
    {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_photo, parent, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int)
    {
        Picasso.get()
            .load(photo_url[position])
            .into(holder.imageView)
    }

    override fun getItemCount(): Int
    {
        return photo_url.size
    }
}