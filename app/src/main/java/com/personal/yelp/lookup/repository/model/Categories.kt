package com.personal.yelp.lookup.repository.model

data class Categories(
    val alias: String,
    val title: String
)
