package com.personal.yelp.lookup.repository.model

data class Businesses(
    val id: String,
    val name: String,

    val alias: String,
    val image_url: String,
    val is_closed: Boolean,
    val url: String,
    val review_count: Int,
    val categories: ArrayList<Categories>,
    val rating: Float,
    val coordinates: Coordinates,
    val location: Location,
    val phone: String,
    val display_phone: String,
    val distance: Double,

    val photos: ArrayList<String>,
    val price: String,
    val hours: ArrayList<WeeklySchedule>
)
{
    fun getFullCategoryString() : String
    {
        var category = ""

        for(c in categories)
        {
            category += if (category.isEmpty())
                            c.title
                        else
                            ", ${c.title}"
        }

        return category
    }
}
