package com.personal.yelp.lookup.repository.model

data class WeeklySchedule(
    val open: ArrayList<Schedule>
)
