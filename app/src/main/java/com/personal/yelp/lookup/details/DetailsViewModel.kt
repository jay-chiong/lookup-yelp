package com.personal.yelp.lookup.details

import android.app.Application
import android.content.Context
import android.graphics.Typeface
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.databinding.ObservableArrayList
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.lifecycle.AndroidViewModel
import com.personal.yelp.lookup.R
import com.personal.yelp.lookup.repository.model.BusinessReview
import com.personal.yelp.lookup.repository.model.Businesses
import com.personal.yelp.lookup.repository.model.WeeklySchedule
import com.personal.yelp.lookup.repository.source.ServerDataSource
import com.squareup.picasso.Picasso
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat

/**
 * Handles the request and process of DetailsFragment
 */
class DetailsViewModel(
    application: Application,
    val context: Context) : AndroidViewModel(application)
{
    private val service = ServerDataSource.create(context)

    private lateinit var detailsCall: Call<Businesses>
    private lateinit var reviewsCall: Call<BusinessReview>

    val businessName = ObservableField<String>()
    val businessCategory = ObservableField<String>()
    val businessAddress1 = ObservableField<String>()
    val businessAddress2 = ObservableField<String>()
    val businessPhoneNumber = ObservableField<String>()
    val businessRating = ObservableField<Float>()

    val firstReviewName = ObservableField<String>()
    val firstReviewRating = ObservableField<Float>()
    val firstReviewDate = ObservableField<String>()
    val firstReviewText = ObservableField<String>()

    val secondReviewName = ObservableField<String>()
    val secondReviewRating = ObservableField<Float>()
    val secondReviewDate = ObservableField<String>()
    val secondReviewText = ObservableField<String>()

    val showFirstReview = ObservableBoolean(false)
    val showSecondReview = ObservableBoolean(false)

    val photoUrls = ObservableArrayList<String>()

    fun setupAcquiredDetails(imageView: ImageView, business: Businesses)
    {
        if (business.image_url.isNotEmpty() && business.image_url != null)
        {
            Picasso.get()
                .load(business.image_url)
                .into(imageView)
        }

        businessName.set(business.name)
        businessCategory.set(business.getFullCategoryString())
        businessRating.set(business.rating)

        if (business.display_phone.isNotEmpty())
            businessPhoneNumber.set(business.display_phone)
        else
            businessPhoneNumber.set("Not available")

        if (business.location.display_address.isNotEmpty())
        {
            if (business.location.display_address.size > 1)
            {
                businessAddress2.set(business.location.display_address[1])
            }

            businessAddress1.set(business.location.display_address[0])
        }
    }

    /**
     * Trigger a request to get all the details of the movie
     * @param keyTitle The title of the movie
     * @param keyYear The year of the movie
     * @param imageView The instance of an imageView on where the image will be displayed
     * @param context The context of the activity
     */
    fun getBusinessDetails(business_id: String, view: LinearLayout)
    {
        if (this::detailsCall.isInitialized)
        {
            detailsCall.cancel()
        }

        detailsCall = service.getBusinessDetails(business_id)
        detailsCall.enqueue(object : Callback<Businesses>{
            override fun onResponse(call: Call<Businesses>, response: Response<Businesses>)
            {
                if (response.isSuccessful)
                {
                    response.body()?.let {
                        if (it.photos.isNotEmpty())
                        {
                            photoUrls.clear()
                            photoUrls.addAll(it.photos)
                        }

                        setupWorkingHours(it.hours, view)
                    }

                    getBusinessReviews(business_id)
                }
                else
                {
                    Log.e("details-error", response.errorBody().toString())
                }
            }

            override fun onFailure(call: Call<Businesses>, t: Throwable)
            {
                Log.e("details-error", t.message.toString())
            }
        })
    }

    private fun getBusinessReviews(business_id: String)
    {
        if (this::reviewsCall.isInitialized)
        {
            reviewsCall.cancel()
        }

        reviewsCall = service.getBusinessReviews(business_id)
        reviewsCall.enqueue(object : Callback<BusinessReview>{
            override fun onResponse(
                call: Call<BusinessReview>,
                response: Response<BusinessReview>
            )
            {
                if (response.isSuccessful)
                {
                    response.body()?.let {
                        if (it.reviews.isNotEmpty())
                        {
                            if (it.reviews.size > 1)
                            {
                                secondReviewName.set(it.reviews[1].user.name)
                                secondReviewRating.set(it.reviews[1].rating)
                                secondReviewText.set(it.reviews[1].text)
                                secondReviewDate.set(it.reviews[1].getParsedDate())
                                showSecondReview.set(true)
                            }

                            firstReviewName.set(it.reviews[0].user.name)
                            firstReviewRating.set(it.reviews[0].rating)
                            firstReviewText.set(it.reviews[0].text)
                            firstReviewDate.set(it.reviews[0].getParsedDate())
                            showFirstReview.set(true)
                        }
                    }
                }
                else
                {
                    Log.e("reviews-error", response.errorBody().toString())
                }
            }

            override fun onFailure(call: Call<BusinessReview>, t: Throwable)
            {
                Log.e("reviews-error", t.message.toString())
            }

        })
    }

    private fun setupWorkingHours(schedule: ArrayList<WeeklySchedule>, view: LinearLayout)
    {
        if (schedule != null)
        {
            if (schedule[0].open.isNotEmpty())
            {
                for (workingHours in schedule[0].open)
                {
                    val textView = TextView(context)
                    textView.textSize = 16F
                    textView.setTextColor(ContextCompat.getColor(context, R.color.black))
                    textView.typeface = Typeface.create("sans-serif-medium", Typeface.NORMAL)
                    textView.text = workingHours.getWorkTime()

                    view.addView(textView)
                }
            }
        }
        else
        {
            val textView = TextView(context)
            textView.textSize = 16F
            textView.setTextColor(ContextCompat.getColor(context, R.color.black))
            textView.typeface = Typeface.create("sans-serif-medium", Typeface.NORMAL)
            textView.setTextColor(ContextCompat.getColor(context, android.R.color.darker_gray))
            textView.text = "Not available"

            view.addView(textView)
        }
    }
}