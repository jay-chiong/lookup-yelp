package com.personal.yelp.lookup.search

import android.annotation.SuppressLint
import android.app.Application
import android.content.Context
import android.content.DialogInterface
import android.location.Geocoder
import android.location.Location
import android.os.Looper
import android.util.Log
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.databinding.ObservableArrayList
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.databinding.ObservableList
import androidx.lifecycle.AndroidViewModel
import com.google.android.gms.location.*
import com.google.android.material.chip.Chip
import com.personal.yelp.lookup.common.Constants
import com.personal.yelp.lookup.common.SingleLiveEvent
import com.personal.yelp.lookup.repository.model.AutoCompleteResult
import com.personal.yelp.lookup.repository.model.Businesses
import com.personal.yelp.lookup.repository.model.SearchResult
import com.personal.yelp.lookup.repository.source.ServerDataSource
import com.personal.yelp.lookup.util.PermissionUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*
import kotlin.collections.ArrayList

/**
 * Handles the request and process of SearchResultFragment
 */
class SearchViewModel(
        application: Application,
        val context: Context)
    : AndroidViewModel(application), TextView.OnEditorActionListener
{
    private val service = ServerDataSource.create(context)

    private lateinit var searchCall: Call<SearchResult>

    lateinit var fusedLocationProviderClient: FusedLocationProviderClient

    private var selectedBusiness: Businesses? = null
    private val copyOfResult = ArrayList<Businesses>()

    val doSearch = SingleLiveEvent<Void>()

    val hasResult = ObservableBoolean(false)
    val isLoading = ObservableBoolean(false)
    val searchItems = ObservableArrayList<Businesses>()
    val isReadyToDisplay = ObservableBoolean(false)
    val showOtherSearchViews = ObservableBoolean(false)

    val location = ObservableField("")
    val category = ObservableField("")

    var currentTextInSearch = ""
    var currentAddress = ""

    val notifyAdapterEvent = SingleLiveEvent<Void>()
    val checkPermissionEvent = SingleLiveEvent<Void>()
    val launchLocationSettingsEvent = SingleLiveEvent<Void>()

    fun getSearchResult(term: String)
    {
        if (this::searchCall.isInitialized)
        {
            searchCall.cancel()
        }

        isLoading.set(true)

        var address = if (location.get().toString().isNotEmpty()) {
            location.get().toString()
        } else {
            currentAddress
        }

        searchCall = service.searchBusiness(term, address, category.get().toString())
        searchCall.enqueue(object : Callback<SearchResult>{
            override fun onResponse(call: Call<SearchResult>, response: Response<SearchResult>)
            {
                if (response.isSuccessful)
                {
                    searchItems.clear()
                    response.body()?.let {
                        searchItems.addAll(it.businesses)
                        copyOfResult.addAll(it.businesses)
                        hasResult.set(it.businesses.isNotEmpty())
                    }
                }
                else
                {
                    Log.e("search-error", response.errorBody().toString())
                }

                isLoading.set(false)
                isReadyToDisplay.set(true)
            }

            override fun onFailure(call: Call<SearchResult>, t: Throwable)
            {
                Log.e("search-error", t.message.toString())
                isLoading.set(false)
                isReadyToDisplay.set(true)
            }
        })
    }

    fun setSelectedBusiness(business: Businesses)
    {
        this.selectedBusiness = business
    }

    fun getSelectedBusiness() : Businesses?
    {
        return this.selectedBusiness
    }

    fun sortResults(view: View)
    {
        searchItems.clear()

        val v = view as Chip

        when (v.text.toString())
        {
            Constants().SORT_DEFAULT -> {
                searchItems.addAll(copyOfResult)
            }

            Constants().SORT_DISTANCE -> {
                val items = ArrayList(copyOfResult)

                items.let { ite ->
                    ite.sortBy {
                        it.distance
                    }
                }

                searchItems.addAll(items)
            }

            Constants().SORT_RATING -> {
                val items = ArrayList(copyOfResult)

                items.let { ite ->
                    ite.sortByDescending {
                        it.rating
                    }
                }

                searchItems.addAll(items)
            }
        }
    }

    fun searchItemsAddOnListChanged()
    {
        searchItems.addOnListChangedCallback(object :
            ObservableList.OnListChangedCallback<ObservableArrayList<Businesses>>() {
            override fun onChanged(sender: ObservableArrayList<Businesses>?) {
                notifyAdapterEvent.call()
            }

            override fun onItemRangeChanged(
                sender: ObservableArrayList<Businesses>?,
                positionStart: Int,
                itemCount: Int
            ) {
                notifyAdapterEvent.call()
            }

            override fun onItemRangeInserted(
                sender: ObservableArrayList<Businesses>?,
                positionStart: Int,
                itemCount: Int
            ) {
                notifyAdapterEvent.call()
            }

            override fun onItemRangeMoved(
                sender: ObservableArrayList<Businesses>?,
                fromPosition: Int,
                toPosition: Int,
                itemCount: Int
            ) {
                notifyAdapterEvent.call()
            }

            override fun onItemRangeRemoved(
                sender: ObservableArrayList<Businesses>?,
                positionStart: Int,
                itemCount: Int
            ) {
                notifyAdapterEvent.call()
            }
        })
    }

    fun showGpsRequestDialog()
    {
        if (!PermissionUtil().isSystemLocationEnabled(context))
        {
            val alertDialogBuilder = AlertDialog.Builder(context)
            alertDialogBuilder
                .setTitle("Request Permission")
                .setMessage("Would you like to enable gps to get your current location?")
                .setPositiveButton("Ok", DialogInterface.OnClickListener { _, _ ->
                    launchLocationSettingsEvent.call()
                })
                .setNegativeButton("Cancel", DialogInterface.OnClickListener { dialog, _ ->
                    dialog.dismiss()
                })
                .show()
        }
        else
        {
            if (PermissionUtil().isLocationPermissionsGranted(context))
            {
                getLastLocation()
            }
        }
    }

    @SuppressLint("MissingPermission")
    fun getLastLocation()
    {
        if(PermissionUtil().isSystemLocationEnabled(context))
        {
            fusedLocationProviderClient.lastLocation.addOnCompleteListener { task ->
                var location: Location? = task.result

                if(location == null)
                {
                    getNewLocation()
                }
                else
                {
                    createAddressString(location.latitude, location.longitude)
                }
            }
        }
        else
        {
            Toast.makeText(context,"Please Turn on Your device Location", Toast.LENGTH_SHORT).show()
        }
    }

    @SuppressLint("MissingPermission")
    fun getNewLocation()
    {
        var locationRequest =  LocationRequest()
        locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        locationRequest.interval = 0
        locationRequest.fastestInterval = 0
        locationRequest.numUpdates = 1
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(context)
        fusedLocationProviderClient!!.requestLocationUpdates(
            locationRequest,locationCallback, Looper.myLooper()
        )
    }

    private val locationCallback = object : LocationCallback()
    {
        override fun onLocationResult(locationResult: LocationResult)
        {
            var lastLocation: Location = locationResult.lastLocation
            createAddressString(lastLocation.latitude, lastLocation.longitude)
        }
    }

    private fun createAddressString(lat: Double, long: Double)
    {
        val geoCoder = Geocoder(context, Locale.getDefault())
        val addresses = geoCoder.getFromLocation(lat,long,3)

        if (addresses.isNotEmpty())
            currentAddress = addresses[0].locality + " " + addresses[0].subAdminArea + " " + addresses[0].countryName
    }

    override fun onEditorAction(v: TextView?, actionId: Int, event: KeyEvent?): Boolean
    {
        if (actionId == EditorInfo.IME_ACTION_SEARCH)
        {
            doSearch.call()
            return true;
        }

        return false
    }
}