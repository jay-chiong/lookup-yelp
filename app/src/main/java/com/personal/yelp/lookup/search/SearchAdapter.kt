package com.personal.yelp.lookup.search

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.RatingBar
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.databinding.ObservableArrayList
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.personal.yelp.lookup.R
import com.personal.yelp.lookup.details.PhotoAdapter
import com.personal.yelp.lookup.repository.model.Businesses
import com.squareup.picasso.Picasso

/**
 * Handles the display of the movies
 */
class SearchAdapter(
    private val result: ObservableArrayList<Businesses>,
    private val activity: FragmentActivity
) : RecyclerView.Adapter<SearchAdapter.ViewHolder>()
{
    var onItemClick: ((Businesses) -> Unit)? = null

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view)
    {
        val bNameTextView: TextView = view.findViewById(R.id.business_name_tv)
        val bAddressTextView: TextView = view.findViewById(R.id.business_address_tv)
        val bCategoryTextView: TextView = view.findViewById(R.id.business_category_tv)
        val bRatingTextView: TextView = view.findViewById(R.id.business_rating_tv)
        val bRDistanceTextView: TextView = view.findViewById(R.id.business_distance_tv)
        val bRatingBar: RatingBar = view.findViewById(R.id.business_rating_rb)

        private val bCardLayout: CardView = view.findViewById(R.id.result_cv)

        init {
            bCardLayout.setOnClickListener{
                onItemClick?.invoke(result[adapterPosition])
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder
    {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_search_result, parent, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int)
    {
        val item = result[position]

        holder.bNameTextView.text = item.name
        holder.bAddressTextView.text = item.location.getFullAddress()
        holder.bCategoryTextView.text = item.getFullCategoryString()
        holder.bRatingTextView.text = item.rating.toString()
        holder.bRDistanceTextView.text = String.format("%.2f mi", item.distance)
        holder.bRatingBar.rating = item.rating
    }

    override fun getItemCount(): Int
    {
        return result.size
    }


}