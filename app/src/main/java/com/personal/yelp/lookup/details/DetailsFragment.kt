package com.personal.yelp.lookup.details

import android.graphics.Typeface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.databinding.ObservableArrayList
import androidx.databinding.ObservableList
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.personal.yelp.lookup.R
import com.personal.yelp.lookup.common.ViewModelFactory
import com.personal.yelp.lookup.databinding.FragmentDetailsBinding
import com.personal.yelp.lookup.search.SearchViewModel


/**
 * Fragment class for showing the other information of the selected result
 */
class DetailsFragment : Fragment()
{
    private lateinit var _binding: FragmentDetailsBinding
    private lateinit var _detailsViewModel: DetailsViewModel
    private lateinit var _searchViewModel: SearchViewModel
    private lateinit var _photosAdapter: PhotoAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View?
    {
        val root: View = inflater.inflate(R.layout.fragment_details, container, false)

        _binding = FragmentDetailsBinding.bind(root)

        setupViewModel()

        return _binding.root
    }

    private fun setupViewModel()
    {
        _detailsViewModel = getDetailsViewModel()
        _searchViewModel = getSearchResultViewModel()
        _photosAdapter = PhotoAdapter(_detailsViewModel.photoUrls)

        _binding.viewModel = _detailsViewModel

        _binding.detailsPhotosRv.layoutManager =
            LinearLayoutManager(requireActivity(), LinearLayoutManager.HORIZONTAL, false)
        _binding.detailsPhotosRv.adapter = _photosAdapter

        _searchViewModel.getSelectedBusiness()?.let {
            _detailsViewModel.setupAcquiredDetails(_binding.detailsIv, it)
            _detailsViewModel.getBusinessDetails(it.id, _binding.workHoursLayout)
        }

        _detailsViewModel.photoUrls.addOnListChangedCallback(object :
            ObservableList.OnListChangedCallback<ObservableArrayList<String>>() {
            override fun onChanged(sender: ObservableArrayList<String>?) {
                _photosAdapter.notifyDataSetChanged()
            }

            override fun onItemRangeChanged(
                sender: ObservableArrayList<String>?,
                positionStart: Int,
                itemCount: Int
            ) {
                _photosAdapter.notifyDataSetChanged()
            }

            override fun onItemRangeInserted(
                sender: ObservableArrayList<String>?,
                positionStart: Int,
                itemCount: Int
            ) {
                _photosAdapter.notifyDataSetChanged()
            }

            override fun onItemRangeMoved(
                sender: ObservableArrayList<String>?,
                fromPosition: Int,
                toPosition: Int,
                itemCount: Int
            ) {
                _photosAdapter.notifyDataSetChanged()
            }

            override fun onItemRangeRemoved(
                sender: ObservableArrayList<String>?,
                positionStart: Int,
                itemCount: Int
            ) {
                _photosAdapter.notifyDataSetChanged()
            }
        })
    }

    private fun getDetailsViewModel() : DetailsViewModel
    {
        val factory = ViewModelFactory(requireActivity().application, requireActivity()).getInstance()

        return ViewModelProvider(this, factory).get(DetailsViewModel::class.java)
    }

    private fun getSearchResultViewModel() : SearchViewModel
    {
        val factory = ViewModelFactory(requireActivity().application, requireActivity()).getInstance()

        return ViewModelProvider(requireActivity(), factory).get(SearchViewModel::class.java)
    }
}