package com.personal.yelp.lookup.common

import android.app.Application
import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.personal.yelp.lookup.details.DetailsViewModel
import com.personal.yelp.lookup.search.SearchViewModel

/**
 * Manage all viewModels
 */
class ViewModelFactory(
    private val application: Application,
    private val context: Context) : ViewModelProvider.NewInstanceFactory()
{
    private lateinit var INSTANCE: ViewModelFactory

    fun getInstance() : ViewModelFactory
    {
        if ( !this::INSTANCE.isInitialized )
        {
            INSTANCE = ViewModelFactory(application, context)
        }

        return INSTANCE
    }

    override fun <T : ViewModel?> create(modelClass: Class<T>): T
    {
        return when {
            modelClass.isAssignableFrom(SearchViewModel::class.java) -> {
                SearchViewModel(application, context) as T
            }
            modelClass.isAssignableFrom(DetailsViewModel::class.java) -> {
                DetailsViewModel(application, context) as T
            }
            else -> throw IllegalArgumentException("Unknown ViewModel class: " + modelClass.name)
        }

    }
}