package com.personal.yelp.lookup.common

class Constants
{
    val SORT_DEFAULT = "Default"
    val SORT_RATING = "Rating"
    val SORT_DISTANCE = "Distance"
}