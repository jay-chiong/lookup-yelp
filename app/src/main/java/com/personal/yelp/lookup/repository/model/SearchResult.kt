package com.personal.yelp.lookup.repository.model

data class SearchResult(
    val businesses: ArrayList<Businesses>,
    val total: Int
)
